package com.timewastingguru.githuber.model.rest;

import com.timewastingguru.githuber.model.entities.ListRepositories;
import com.timewastingguru.githuber.model.entities.ListUsers;
import com.timewastingguru.githuber.model.entities.Repository;
import com.timewastingguru.githuber.model.entities.User;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by stepanych on 12.11.15.
 */
public interface GitHubApi {

    @GET("/search/repositories")
    Observable<ListRepositories> getSearchRepositories(@Query("q") String query);

    @GET("/search/users")
    Observable<ListUsers> getSearchUsers(@Query("q") String query);

}
