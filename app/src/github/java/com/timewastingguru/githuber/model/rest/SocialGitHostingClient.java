package com.timewastingguru.githuber.model.rest;

import com.squareup.okhttp.OkHttpClient;
import com.timewastingguru.githuber.model.entities.ListRepositories;
import com.timewastingguru.githuber.model.entities.ListUsers;
import com.timewastingguru.githuber.model.entities.Repository;
import com.timewastingguru.githuber.model.entities.User;

import java.util.List;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by stepanych on 12.11.15.
 */
public class SocialGitHostingClient extends BaseGitHostingClient<User, Repository> {

    OkHttpClient client = new OkHttpClient();

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(NetworkConfigs.ENDPOINT)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .build();

    GitHubApi mSocialGitHostingApi = retrofit.create(GitHubApi.class);

    @Override
    Observable<List<User>> searchUsers(String query) {
        return mSocialGitHostingApi.getSearchUsers(query)
                .flatMap(new Func1<ListUsers, Observable<List<User>>>() {
            @Override
            public Observable<List<User>> call(ListUsers listUsers) {
                return  Observable.just(listUsers.getItems());
            }
        });
    }

    @Override
    Observable<List<Repository>> searchRepositories(String query) {
        return mSocialGitHostingApi.getSearchRepositories(query)
                .flatMap(new Func1<ListRepositories, Observable<List<Repository>>>() {
                    @Override
                    public Observable<List<Repository>> call(ListRepositories listRepositories) {
                        return Observable.just(listRepositories.getItems());
                    }
                });
    }
}
