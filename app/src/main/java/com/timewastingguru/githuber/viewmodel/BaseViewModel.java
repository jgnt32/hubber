package com.timewastingguru.githuber.viewmodel;

/**
 * Created by stepanych on 03.02.16.
 */
public interface BaseViewModel {

    void onCreate();

    void onDestroy();
}
