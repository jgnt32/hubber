package com.timewastingguru.githuber.viewmodel;

import android.util.Pair;

import com.timewastingguru.githuber.model.entities.Repository;
import com.timewastingguru.githuber.model.entities.User;
import com.timewastingguru.githuber.model.rest.GitHostingService;
import com.timewastingguru.githuber.view.UserReposView;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

/**
 * Created by stepanych on 12.11.15.
 */
public class UserReposViewModel implements BaseViewModel {

    private final List<User> mUsers = new ArrayList<>();
    private final List<Repository> mRepositories = new ArrayList<>();

    private final GitHostingService mGitHostingService;
    private Subscription mSubscription;

    private SoftReference<UserReposView> mUserReposView;
    public UserReposViewModel(UserReposView mUserReposView, GitHostingService gitHostingService) {
        this.mGitHostingService = gitHostingService;
        this.mUserReposView = new SoftReference<>(mUserReposView);
    }

    public void search(String query){
        mSubscription = Observable.zip(mGitHostingService.searchUsers(query),
                mGitHostingService.searchRepositories(query),
            new Func2<List<User>, List<Repository>, Pair<List<User>,List<Repository>>>() {
                @Override
                public Pair<List<User>,List<Repository>> call(List<User> users, List<Repository> repositories) {
                    return new Pair<>(users, repositories);
                }
            }).delay(500, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Pair<List<User>,List<Repository>>>() {
                    @Override
                    public void call(Pair<List<User>,List<Repository>> pair) {
                        mUsers.clear();
                        mUsers.addAll(pair.first);

                        mRepositories.clear();
                        mRepositories.addAll(pair.second);

                        if (mUserReposView.get() != null) {
                            mUserReposView.get().onSearchResults();
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        if (mUserReposView.get() != null) {
                            mUserReposView.get().onError();
                        }
                    }
                });
    }

    @Override
    public void onCreate(){

    }

    @Override
    public void onDestroy(){
        if (mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        mUserReposView = null;
    }

    public List<User> getUsers() {
        return mUsers;
    }

    public List<Repository> getRepositories() {
        return mRepositories;
    }
}
