package com.timewastingguru.githuber.view;

/**
 * Created by stepanych on 12.11.15.
 */
public interface UserReposView {

    void onSearchResults();

    void onError();

}
