package com.timewastingguru.githuber.view.binding;

import android.support.v7.widget.SearchView;

import rx.Observable;
import rx.Subscriber;

/**
 * https://github.com/JakeWharton/RxBinding/blob/master/rxbinding-appcompat-v7/src/main/java/com/jakewharton/rxbinding/support/v7/widget/SearchViewQueryTextChangesOnSubscribe.java
 */
public class SearchViewQueryTextChangesOnSubscribe implements Observable.OnSubscribe<CharSequence> {

        private final SearchView view;

        public SearchViewQueryTextChangesOnSubscribe(SearchView view) {
            this.view = view;
        }

        @Override public void call(final Subscriber<? super CharSequence> subscriber) {

            SearchView.OnQueryTextListener watcher = new SearchView.OnQueryTextListener() {
                @Override public boolean onQueryTextChange(String s) {
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onNext(s);
                        return true;
                    }
                    return false;
                }

                @Override public boolean onQueryTextSubmit(String query) {
                    return false;
                }
            };

            view.setOnQueryTextListener(watcher);

            subscriber.add(new MainThreadSubscription() {
                @Override protected void onUnsubscribe() {
                    view.setOnQueryTextListener(null);
                }
            });

            // Emit initial value.
            subscriber.onNext(view.getQuery());
        }
}

