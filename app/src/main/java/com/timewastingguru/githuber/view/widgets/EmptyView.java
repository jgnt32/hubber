package com.timewastingguru.githuber.view.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Created by stepanych on 13.11.15.
 */
public class EmptyView extends FrameLayout {

    private final ProgressBar mProgressBar;
    private final TextView mTextView;

    {
        mProgressBar = new ProgressBar(getContext());
        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
        mProgressBar.setLayoutParams(params);

        mTextView = new TextView(getContext());
        mTextView.setLayoutParams(params);
        mTextView.setVisibility(INVISIBLE);
        addView(mProgressBar);
        addView(mTextView);
    }

    public EmptyView(Context context) {
        super(context);
    }

    public EmptyView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EmptyView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void showProgressBar(){
        mTextView.setVisibility(INVISIBLE);
        mProgressBar.setVisibility(VISIBLE);
    }

    public void showText(String message){
        mTextView.setVisibility(VISIBLE);
        mTextView.setText(message);
        mProgressBar.setVisibility(INVISIBLE);
    }

    public void showText(int message){
        mTextView.setVisibility(VISIBLE);
        mProgressBar.setVisibility(INVISIBLE);
        mTextView.setText(message);
    }

    public void hide(){
        setVisibility(INVISIBLE);
    }

}
