package com.timewastingguru.githuber.view.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.timewastingguru.githuber.R;
import com.timewastingguru.githuber.model.entities.Repository;
import com.timewastingguru.githuber.model.entities.User;


import java.util.List;

/**
 * Created by stepanych on 12.11.15.
 */
public class UserReposListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int USER = 0;
    public static final int REPOSITORY = 1;

    private final List<User> mUsers;
    private final List<Repository> mRepositories;

    public UserReposListAdapter(List<User> users, List<Repository> repositories) {
        this.mUsers = users;
        this.mRepositories = repositories;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        if (viewType == USER) {
            View itemView = layoutInflater.inflate(R.layout.user_list_item, parent, false);
            return new UserHolder(itemView);
        } else {
            View itemView = layoutInflater.inflate(R.layout.repository_list_item, parent, false);
            return new RepositoryViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == USER) {
            ((UserHolder)holder).populateView(getUser(position));
        } else {
            ((RepositoryViewHolder)holder).populateView(getRepository(position));
        }
    }

    private User getUser(int position){
        int userIndex = position < mRepositories.size() ? position / 2 : position - mRepositories.size();
        return mUsers.get(userIndex);
    }

    private Repository getRepository (int position){
        int repoIndex = position < mUsers.size() ? position / 2 : position - mUsers.size();
        return mRepositories.get(repoIndex);
    }

    @Override
    public int getItemCount() {
        return mUsers.size() + mRepositories.size();
    }


    @Override
    public int getItemViewType(int position) {
        if (position % 2 == 0) {
            return position / 2 >= mUsers.size() ? REPOSITORY : USER;
        } else {
            return position / 2 >= mRepositories.size() ? USER : REPOSITORY;
        }
    }

    public static class UserHolder extends RecyclerView.ViewHolder{

        private TextView mLogin;
        private ImageView mAvatar;


        public UserHolder(View itemView) {
            super(itemView);
            mLogin = (TextView) itemView.findViewById(R.id.user_login);
            mAvatar = (ImageView) itemView.findViewById(R.id.avatar);
        }

        public void populateView(User user){
            mLogin.setText(user.getLogin());
            Glide.with(mAvatar.getContext()).load(user.getAvatarURL()).into(mAvatar);

        }
    }

    public static class RepositoryViewHolder extends RecyclerView.ViewHolder {

        private TextView mTitle;
        private TextView mDescription;
        private TextView mForks;

        public RepositoryViewHolder(View itemView) {
            super(itemView);
            mTitle = (TextView) itemView.findViewById(R.id.repository_title);
            mDescription = (TextView) itemView.findViewById(R.id.repository_description);
            mForks = (TextView) itemView.findViewById(R.id.forks_count);
        }

        void populateView(Repository repository){
            mTitle.setText(repository.getTitle());
            mDescription.setText(repository.getDescription());
            mForks.setText(Integer.toString(repository.getForksCount()));
        }
    }

}
