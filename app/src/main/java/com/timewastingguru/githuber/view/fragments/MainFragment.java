package com.timewastingguru.githuber.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.timewastingguru.githuber.R;
import com.timewastingguru.githuber.model.rest.GitHostingService;
import com.timewastingguru.githuber.model.rest.SocialGitHostingClient;
import com.timewastingguru.githuber.view.binding.SearchViewQueryTextChangesOnSubscribe;
import com.timewastingguru.githuber.view.UserReposView;
import com.timewastingguru.githuber.view.adapters.UserReposListAdapter;
import com.timewastingguru.githuber.view.widgets.EmptyView;
import com.timewastingguru.githuber.viewmodel.UserReposViewModel;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Created by stepanych on 12.11.15.
 */
public class MainFragment extends Fragment implements UserReposView {

    private UserReposViewModel mViewModel;
    private UserReposListAdapter mAdapter;

    private SearchView mSearchView;
    private RecyclerView mRecyclerView;
    private EmptyView mEmptyView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
        mViewModel = new UserReposViewModel(this, new GitHostingService(new SocialGitHostingClient()));
        mViewModel.onCreate();
        mAdapter = new UserReposListAdapter(mViewModel.getUsers(), mViewModel.getRepositories());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) MenuItemCompat.getActionView(item);
        Observable.create(new SearchViewQueryTextChangesOnSubscribe(mSearchView))
                .debounce(1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<CharSequence>() {
                    @Override
                    public void call(CharSequence charSequence) {
                        if (charSequence.length() > 0) {
                            mViewModel.search(charSequence.toString());
                            mEmptyView.setVisibility(View.VISIBLE);
                            mEmptyView.showProgressBar();
                        }
                    }
                });

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_repos, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mAdapter);
        mEmptyView = (EmptyView) view.findViewById(R.id.empty_view);
        mEmptyView.showText(getString(R.string.start_message));
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mViewModel.onDestroy();
    }

    @Override
    public void onSearchResults() {
        mAdapter.notifyDataSetChanged();
        if (mViewModel.getRepositories().isEmpty() && mViewModel.getUsers().isEmpty()) {
            mEmptyView.showText(getString(R.string.no_results));
        } else {
            mEmptyView.hide();

        }
    }

    @Override
    public void onError() {
        mEmptyView.showText(getString(R.string.error));
    }
}
