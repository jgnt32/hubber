package com.timewastingguru.githuber.model.rest;

import com.timewastingguru.githuber.model.entities.Repository;
import com.timewastingguru.githuber.model.entities.User;

import java.util.List;

import rx.Observable;

/**
 * Created by stepanych on 03.02.16.
 */
public abstract class BaseGitHostingClient {

    abstract Observable<List<User>> searchUsers(final String query);

    abstract Observable<List<Repository>> searchRepositories(final String query);


}
