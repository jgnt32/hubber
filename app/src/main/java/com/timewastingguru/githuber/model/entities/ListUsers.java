package com.timewastingguru.githuber.model.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by stepanych on 12.11.15.
 */
public class ListUsers {

    @SerializedName("items")
    List<User> mItems;

    public List<User> getItems() {
        return mItems;
    }

    public void setItems(List<User> mItems) {
        this.mItems = mItems;
    }
}
