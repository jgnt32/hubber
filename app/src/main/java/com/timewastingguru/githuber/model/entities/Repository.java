package com.timewastingguru.githuber.model.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by stepanych on 12.11.15.
 */
public class Repository{

    @SerializedName("name")
    private String mTitle;

    @SerializedName("forks_count")
    private int mForksCount;

    @SerializedName("description")
    private String mDescription;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public int getForksCount() {
        return mForksCount;
    }

    public void setForksCount(int forksCount) {
        mForksCount = forksCount;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }
}
