package com.timewastingguru.githuber.model.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by stepanych on 12.11.15.
 */
public class ListRepositories {

    @SerializedName("items")
    private List<Repository> mItems;

    public List<Repository> getItems() {
        return mItems;
    }

    public void setItems(List<Repository> items) {
        this.mItems = items;
    }
}


