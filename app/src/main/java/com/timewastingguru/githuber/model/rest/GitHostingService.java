package com.timewastingguru.githuber.model.rest;

import com.timewastingguru.githuber.model.entities.Repository;
import com.timewastingguru.githuber.model.entities.User;

import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by stepanych on 12.11.15.
 */
public class GitHostingService {

    private BaseGitHostingClient mGitHostingClient;

    public GitHostingService(BaseGitHostingClient mGitHostingClient) {
        this.mGitHostingClient = mGitHostingClient;
    }

    public Observable<List<User>> searchUsers(final String query){
        return mGitHostingClient.searchUsers(query)
                .compose(this.<List<User>>applyDefaultConfiguration());
    }

    public Observable<List<Repository>> searchRepositories(final String query){
        return mGitHostingClient.searchRepositories(query)
                .compose(this.<List<Repository>>applyDefaultConfiguration());
    }

    private <T> Observable.Transformer<T, T> applyDefaultConfiguration() {
        return new Observable.Transformer<T, T>() {
            @Override
            public Observable<T> call(Observable<T> tObservable) {
                return tObservable
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread());
            }
        };
    }

}
