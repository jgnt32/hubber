package com.timewastingguru.githuber.model.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by stepanych on 12.11.15.
 */
public class User {

    @SerializedName("login")
    private String mLogin;

    @SerializedName("avatar_url")
    private String mAvatarUrl;

    public String getLogin() {
        return mLogin;
    }

    public void setLogin(String login) {
        mLogin = login;
    }

    public String getAvatarURL() {
        return mAvatarUrl;
    }

    public void setAvatarUrl(String url) {
        mAvatarUrl = url;
    }
}
